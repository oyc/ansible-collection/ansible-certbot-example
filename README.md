# ansible-certbot-example

Setup and renew let's encrypt certificates with Ansible.

The Ansible role used to generate certificates comes from the repository [thefinn93/ansible-letsencrypt](https://github.com/thefinn93/ansible-letsencrypt):

> This repository is just a small wrapper around already existing code to show how to generate certificates with Ansible
> More information on what is an Ansible role: https://docs.ansible.com/ansible/2.7/user_guide/playbooks_reuse_roles.html

## Quick Start with Apache2

This quickstart tutorial assumes that you have a running `apache2` server instance on a remote host
and a local host that will run ansible.

It also assumes that port 80 and 443 on your remote host are open and reachable and you can connect to it through `ssh` using `root` acccount by default or any other user if you modify `inventories/cloud-vps.yml`

### Prerequisites on **remote host**

Before generating LetsEncrypt certificates you must check that:

- **SSL mode is enabled** in you run an Apache2 server
```bash
sudo a2enmod ssl
```

- **A configuration allowing TLS encryption and HTTPS protocol** exists in `sites-enabled`
If you do not have one, use the example present at installation:
```bash
cp /etc/apache2/sites-available/default-ssl.conf /etc/apache2/sites-enabled/default-ssl.conf
```

Keep the file untouch for the moment.

### Dependencies on **local host**

#### Clone the git repository

```bash
git clone https://gitlab.com/oyc/ansible-collection/ansible-certbot-example
```

This guide depends on two python packages:

- `ansible` [Ansible can help you with configuration management, application deployment, task automation](https://www.ansible.com/overview/how-ansible-works)
- `invoke`: [Invoke is a Python (2.7 and 3.4+) task execution tool & library](https://www.pyinvoke.org/)


#### Install pip and virtualenv

To install them, first install `pip` for your current user if it's not the case yet:
```bash
wget https://bootstrap.pypa.io/get-pip.py -O $HOME/get-pip.py
python $HOME/get-pip.py --user
```

Check your `pip` installation with:
```bash
pip --version
```

Once `pip` is installed, you can use a `virtualenv` to install dependencies inside a clean
environment.
```bash
pip install --user virtualenv
```

Check your virtualenv installation
```bash
virtualenv --version
```

#### Install latest version of Ansible and Invoke

1) Create your virtualenv in the directory of your choice.

I will create it in my home directory
```bash
cd
virtualenv .ansible_env
cd -
```

Once the virtualenv is created, you can enable it by sourcing the activate file, and install dependencies listed in `requirements.txt` file of this repository:
```bash
. $HOME/.ansible_env/bin/activate
pip install -r requirements.txt
```

### Modify inventory

The hostnames and IPV4 addresses of remote hosts are defined in `inventories/cloud-vps.yml`.
You must modify this file accordingly.

3 variables are defined for the remote host:

- **ansible_host**: IPV4 address
- **ansible_user**: User that will be used by ansible to connect to remote host (using SSH)
- **ansible_python_interpreter**: Path of python interpreter on remote host that should be used by Ansible


> More info on Ansible inventories: https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html

### Modify host variables

Variables used to configure certificates generation are defined per host in the `playbooks/host_vars/` directory.
Create a new file similar to `capdatalab.xyz.yml` which is the file holding variables
for host capdatalab.xyz.

> capdatalab.xyz is a domain name that I own and that I use to make tests with certificates

Name your file with the name of your host and change the valus of variables accordingly.

> You can check how the `ansible-letsencrypt` role works in the [GitHub repo](https://github.com/thefinn93/ansible-letsencrypt)

> See documentation on splitting out host and group specific data with Ansible: https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html#splitting-out-host-and-group-specific-data

## Run the playbook

Make sure your virtual environment is still activated and run:

```bash
invoke play
```
It runs the `play` task defined in `tasks.py` file with its default arguments:
```bash
invoke play --inventory cloud-vps.yml --playbook letsencrypt_webservers.yml
# or
# invoke play -i cloud-vps.yml -p letsencrypt_webservers.yml
```

> You can see the actual bash command being ran by invoke displayed first in stdout

You will see the status of each steps inside the playbook, either:
- OK: Desired state without changes
- Changed: Desired state with changes
- Failed: Cannot make it into desited state


### Results of playbook (on remote host)

At the end of the playbook, a directory has been created in `/etc/letsencrypt/live/`
with the name of the domain for which you requested certificates.

You also have a new cron job named `Ansible: Let's Encrypt Renewal` inside `/var/spool/cron/crontabs/root` with the following content:
```
# DO NOT EDIT THIS FILE - edit the master and reinstall.
# (/tmp/crontabvavn038n installed on Sat Dec  1 15:39:26 2018)
# (Cron version -- $Id: crontab.c,v 2.13 1994/01/17 03:20:37 vixie Exp $)
#Ansible: Let's Encrypt Renewal
0 */12 * * * /usr/bin/letsencrypt renew --quiet --renew-hook "systemctl restart apache2"
```
> Note: If ansible_user is set to another user it will be inside crontabs for this user.

It should work fine as it is but you can customize it if you want.

- It will try to renew every certificates every 12 hours.
- Only certificates that will expire in less that 30 days will be renewed

#### Updating configuration with HTTPS

All you need to have a valid HTTPS connection on your website now is to update apache2 configuration:

```bash
vi /etc/apache2/sites-enabled/default-ssl.conf
```

Find the lines declaring path to certificates and modify them:
```
#   A self-signed (snakeoil) certificate can be created by installing
#   the ssl-cert package. See
#   /usr/share/doc/apache2/README.Debian.gz for more info.
#   If both key and certificate are stored in the same file, only the
#   SSLCertificateFile directive is needed.
SSLCertificateFile      /etc/letsencrypt/live/capdatalab.xyz/fullchain.pem
SSLCertificateKeyFile /etc/letsencrypt/live/capdatalab.xyz/privkey.pem
```

Reload your apache2 server (`systemctl reload apache2`) and you should be able to connect using https.

You can add new tasks in the `tasks.py` to automate even more things.
